"use strict";

// Load plugins
const browsersync = require("browser-sync").create();
const del = require("del");
const gulp = require("gulp");
const merge = require("merge-stream");
const sass = require("gulp-sass");
const cssnano = require("gulp-cssnano");
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const purgecss = require('gulp-purgecss');
const autoprefixer = require('gulp-autoprefixer');

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./"
    },
    port: 3000
  });
  done();
}

// BrowserSync reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean vendor
function clean() {
  return del(["./vendor/"]);
}

// Bring third party dependencies from node_modules into vendor directory
function modules() {
  // Build CSS
  var css = gulp.src(['./scss/custom.scss'], { allowEmpty: true })
    .pipe(sourcemaps.init())
    .pipe(sass().on('error',sass.logError))
    .pipe(purgecss({content: ['./index.html']}))
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(rename('custom.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./css'));
  // Bootstrap
  var bootstrap = gulp.src(['./node_modules/bootstrap/dist/js/*'])
    .pipe(gulp.dest('./js'));
  // jQuery
  var jquery = gulp.src([
      './node_modules/jquery/dist/*',
      '!./node_modules/jquery/dist/core.js'
    ])
    .pipe(gulp.dest('./js'));
  return merge(css, bootstrap, jquery);
}

// Watch files
function watchFiles() {
  gulp.watch("./**/*.scss", modules);
  gulp.watch("./**/*.css", browserSyncReload);
  gulp.watch("./**/*.html", browserSyncReload);
}

// Define complex tasks
const vendor = gulp.series(clean, modules);
const build = gulp.series(vendor);
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// Export tasks
exports.clean = clean;
exports.vendor = vendor;
exports.build = build;
exports.watch = watch;
exports.default = build;
